package com.guigu.gulimall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 1. 如何使用Nacos作为配置中心统一管理配置
 * 2. 引入依赖
 *      <dependency>
 *          <groupId>com.alibaba.cloud</groupId>
 *          <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
 *      </dependency>
 * 3. 创建一个bootstrap.properties
 *      spring.application.name=gulimall-coupon
 *      spring.cloud.nacos.config.server-addr=192.168.15.128:8848
 * 4. 需要给配置中心默认添加一个叫 数据集(Data Id) gulimall-coupon.properties，默认规则：应用名.properties
 * 5. 给应用名.properties添加任何配置
 * 6. 动态获取配置
 *      @RefreshScope 动态刷新配置
 *      @Value("${配置项名}")   获取到配置
 *      如果配置中心和当前应用的配置文件中都配置了相同项，优先使用配置中心项
 * 细节
 * 1. 命名空间 的 谜底 配置隔离
 *      默认：public（保留空间），默认新增所有的配置都在public空间
 *      1. 开发，测试，生成，利用命名空间来做环境隔离
 *          注意，在bootstrap.properties配置上，需要使用哪个命名空间下的配置
 *          spring.cloud.nacos.config.namespace=d0e16ce9-1992-4e94-b2e0-cd84208dfa29
 *      2. 每一个微服务之间相互隔离配置，每一个微服务都创建自己的命名空间，只加载自己命名空间下的所有配置
 * 2. 配置集，所有的配置的集合
 * 3. 配置集ID，类似文件名
 *      Data Id
 * 4. 配置分组
 *      默认所有的配置集都属于，DEFAULT_GROUP
 *      1111，618,1212
 *
 * 每个微服务创建自己的命名空间，使用配置分组区分环境, dev、test、prod
 *
 * 同时加载多个配置集
 * 1. 微服务任何配置信息，任何配置文件都可以放在配置中心
 * 2. 只需要再bootstrap.properties说明加载配置中心那些配置文件即可
 * 3. @value、@ConfigurationProperties。。。
 *      以前SpringBoot任何方法从配置文件中获取值，都能使用，配置中心有的优先使用配置中心的
 */
@EnableDiscoveryClient
@SpringBootApplication
public class GulimallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallCouponApplication.class, args);
    }

}
