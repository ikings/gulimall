package com.guigu.gulimall.coupon.dao;

import com.guigu.gulimall.coupon.entity.SkuLadderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品阶梯价格
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-08-26 11:41:17
 */
@Mapper
public interface SkuLadderDao extends BaseMapper<SkuLadderEntity> {
	
}
