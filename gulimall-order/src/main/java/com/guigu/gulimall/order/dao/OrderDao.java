package com.guigu.gulimall.order.dao;

import com.guigu.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-08-26 16:07:28
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
