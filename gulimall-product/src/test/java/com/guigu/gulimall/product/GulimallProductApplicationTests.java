package com.guigu.gulimall.product;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.GetObjectRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.guigu.gulimall.product.entity.BrandEntity;
import com.guigu.gulimall.product.service.BrandService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
class GulimallProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Test
    void testUpload() throws FileNotFoundException
    {
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint = "http://oss-cn-beijing.aliyuncs.com";
        // 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建。
        String accessKeyId = "LTAI4G2yyJ2a2sD2jzoEcWVA";
        String accessKeySecret = "QKCNzNVKUGOgbvfn6YGD9H1gpVLFki";

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        // 上传文件流。
        InputStream inputStream = new FileInputStream("C:\\Users\\Kings\\Desktop\\Redis集群.png");
        ossClient.putObject("kings-mall-images", "Redis集群.png", inputStream);

        // 关闭OSSClient。
        ossClient.shutdown();
    }

    @Autowired
    OSSClient ossClient;

    @Test
    void testUpload2() throws FileNotFoundException {
        // 上传文件流。
        InputStream inputStream = new FileInputStream("C:\\Users\\Kings\\Desktop\\Redis集群.png");
        ossClient.putObject("kings-mall-images", "Redis集群2.png", inputStream);
    }

    @Test
    void contextLoads() {

        BrandEntity entity = new BrandEntity();
        /* 新增
        entity.setName("华为");
        entity.setDescript("国人的品牌");
        brandService.save(entity);
        System.out.println("保存成功...");
        */
        /* 更新
        entity.setBrandId(1L);
        entity.setDescript("华人最屌品牌");
        brandService.updateById(entity);
        */
        List<BrandEntity> list = brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id", 1L));
        System.out.println(list);
    }

}
