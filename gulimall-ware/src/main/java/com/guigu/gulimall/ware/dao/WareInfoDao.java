package com.guigu.gulimall.ware.dao;

import com.guigu.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-08-26 16:11:34
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
